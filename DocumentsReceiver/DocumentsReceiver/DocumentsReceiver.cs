﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

namespace Documents
{
    public delegate void DocumentsReadyHandler(object obj, FileSystemEventArgs e);
    public delegate void TimeOutHandler();

    public class DocumentsReceiver : IDisposable
    {
        public event DocumentsReadyHandler DocumentsReady;
        public event TimeOutHandler TimedOut;
 
        private readonly Timer _timer;
        private readonly FileSystemWatcher _fileSystemWatcher;
        private Dictionary<string, bool> _files = new Dictionary<string, bool>();
        private bool _isWorking;
        public DocumentsReceiver(IEnumerable<string> filesWatching)
        {
            _timer = new Timer();
            _fileSystemWatcher = new FileSystemWatcher();
            filesWatching.ToList().ForEach(f => _files.Add(f, false));
            _isWorking = false;
        }
        public void Dispose()
        {
            _fileSystemWatcher.Changed -= PathChangedHendler;
            _fileSystemWatcher.Dispose();

            _timer.Elapsed -= timerElapsed;
            _timer.Dispose();
        }
        public void Start(string targetDirectory, int waitingInterval)
        {
            _timer.Stop();
            _fileSystemWatcher.Path = targetDirectory;
            _fileSystemWatcher.EnableRaisingEvents = true;
            _fileSystemWatcher.Created += CreateDelete;
            _fileSystemWatcher.Deleted += CreateDelete;

            _timer.Interval = waitingInterval;
            _timer.Elapsed += timerElapsed;
            if (!_isWorking)
            {
                _fileSystemWatcher.Changed += PathChangedHendler;
                _timer.Elapsed += TimerCallbackHendler;

            }

            _timer.Start();
        }

        private void CreateDelete(object sender, FileSystemEventArgs e)
        {
            if (_files.ContainsKey(e.Name))
            {
                _files[e.Name] = (e.ChangeType == WatcherChangeTypes.Created);
                DocumentsReady?.Invoke(this, e);
                if (FilesAllLoaded)
                    UnsetSubscribtion();

            }
        }
        private void PathChangedHendler(object sender, FileSystemEventArgs e)
        {
            foreach (var f in _files)
            {
                if (!File.Exists(_fileSystemWatcher.Path + f))
                {
                    return;
                }
            }

            if (_isWorking)
            {
                StopReceiver();
            }
        }
        private void StopReceiver()
        {
            if (_isWorking)
            {
                _isWorking = false;
                _fileSystemWatcher.EnableRaisingEvents = false;
                _fileSystemWatcher.Changed -= PathChangedHendler;
                _timer.Stop();
                _timer.Elapsed -= TimerCallbackHendler;
            }
        }
        private void TimerCallbackHendler(object sender, ElapsedEventArgs e)
        {
            if (_isWorking)
            {
                StopReceiver();
                TimedOut();
            }
        }


        private void UnsetSubscribtion()
        {
            _timer.Elapsed -= timerElapsed;
            _fileSystemWatcher.Created -= CreateDelete;
            _fileSystemWatcher.Deleted -= CreateDelete;
        }

        private void timerElapsed(object sender, ElapsedEventArgs e)
        {
            TimedOut?.Invoke();
            UnsetSubscribtion();
        }

        public bool FilesAllLoaded { get => _files.Values.All(x => x == true); }
    }
}